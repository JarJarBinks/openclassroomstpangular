export class AppareilService {
    appareils = [
        {
            name: 'oui',
            status: 'éteint'
        },
        {
            name: 'non',
            status: 'éteint'
        },
        {
            name: 'peut-être',
            status: 'éteint'
        }
    ];

    switchOnAll() {
        for(let appareil of this.appareils) {
            appareil.status = 'allumé';
        }
    }

    switchOffAll() {
        for(let appareil of this.appareils) {
            appareil.status = 'éteint';
        }
    }

    switchOnOne(index: number) {
        this.appareils[index].status = 'allumé';
    }

    switchOffOne(index: number) {
        this.appareils[index].status = 'éteint'
    }
}
